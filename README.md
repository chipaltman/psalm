# The 30-Day Cycle of Psalms

Read the Psalms according to the traditional 30-day cycle.  

**SPOILER ALERT:** It is the whole thing in a month *(i.e. hard mode, a slog).*

```mono
psalm 0.1.#

USAGE:
    psalm | less

```
# Example

On the fifth day of each month:
```mono
Day V : Morning Prayer
    
Psalm 24 - Domini est terra
    
1   The earth is the Lord’s and all that is in it, +
    the compass of the world and those who dwell therein.
    
--snikt--

Day V : Evening Prayer
    
Psalm 27 - Dominus illuminatio
    
1   The Lord is my light and my salvation; whom then shall I fear? +
    The Lord is the strength of my life; of whom then shall I be afraid?
```

Psalm text taken from the *New Coverdale Psalter, The Book of Common Prayer (2019).*  
*Copyright © 2019 by the Anglican Church in North America*

<http://bcp2019.anglicanchurch.net/>

Use of Psalms in the Daily Office
----------------------------------

> The recitation of the Psalms is central to daily worship throughout the whole of Christian Tradition. Anglicanism at the time of the Reformation established that the entire Psalter should be read in the Daily Office once each month, according to the pattern printed with this Lectionary. Contemporary practice sometimes lessens the number of the daily psalms, which practice is permissible as long as the entire Psalter is regularly read. For any day, the psalms appointed may be reduced in number according to local circumstance. If there is only one office, the psalms may be drawn from those appointed for Morning or Evening that Day. If a two month cycle of psalms is desired, the Morning psalms may be read in one month and the Evening psalms in the next. When there is a thirty-first day of the month, psalms from the Psalms of Ascents, Psalms 120 to 134, are used.  
>
>*From THE BOOK OF COMMON PRAYER (2019)*  
>*Copyright © 2019 by the Anglican Church in North America*
