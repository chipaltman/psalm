
Psalm 45 - Eructavit cor meum

1   My heart overflows with a noble song; +
    I will sing my words to the King; my tongue is the pen of a ready writer.

2   You are fairer than the children of men; +
    full of grace are your lips, because God has blessed you for ever.

3   Gird your sword upon your thigh, most mighty one, +
    according to your honor and majesty.

4   In your majesty be victorious; ride out for the sake of truth, to bear witness to righteousness, +
    and your right hand shall show you marvelous things.

5   Your arrows are very sharp in the heart of the King’s enemies, +
    and the peoples shall be subdued under you.

6   Your throne, O God, endures for ever; +
    the scepter of your kingdom is a righteous scepter.

7   You have loved righteousness and hated iniquity; +
    therefore God, your God, has anointed you with the oil of gladness above your fellows.

8   All your garments smell of myrrh, aloes, and cassia; +
    out of the ivory palaces, stringed instruments have made you glad.

9   Kings’ daughters are among your honorable women; +
    at your right hand stands the queen in a vesture of gold, wrought with many colors.

10  Hearken, O daughter, and consider; incline your ear; +
    forget your own people, and your father’s house.

11  So shall the King have pleasure in your beauty; +
    since he is your Lord, honor him.

12  And the daughter of Tyre shall bring you gifts; +
    the rich also among the peoples shall seek your favor.

13  The King’s daughter is all glorious within the palace; +
    her clothing is of wrought gold.

14  She shall be brought to the king in embroidered raiment; +
    the virgins who are her companions shall bring her to you.

15  With joy and gladness shall they bring her, +
    and shall enter into the King’s palace.

16  Instead of your fathers, you shall have sons, +
    whom you shall make princes in all the land.

17  I will make your Name to be remembered from one generation to another; +
    therefore the peoples shall praise you, world without end.
