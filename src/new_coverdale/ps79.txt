
Day XVI : Morning Prayer

Psalm 79 - Deus, venerunt

1   O God, the nations have come into your inheritance; +
    they have defiled your holy temple, and made Jerusalem a heap of stones.

2   The dead bodies of your servants have they given to be meat for the birds of the air, +
    and the flesh of your saints to the beasts of the land.

3   Their blood have they shed like water on every side of Jerusalem, +
    and there was no one to bury them.

4   We have become a reproach to our enemies, +
    an object of scorn and derision to those who are round about us.

5   O Lord, how long will you be angry? +
    Shall your jealousy burn like fire for ever?

6   Pour out your indignation upon the nations that have not known you, +
    and upon the kingdoms that have not called upon your Name.

7   For they have devoured Jacob +
    and laid waste his dwelling place.

8   O remember not our past sins, but have mercy on us speedily, +
    for we have come to great misery.

9   Help us, O God of our salvation, for the glory of your Name; +
    O deliver us and forgive our sins for your Name’s sake.

10  Why do the nations say, +
    “Where now is their God?”

11  O let the vengeance of your servants’  blood that is shed +
    be known in our sight among the nations.

12  O let the sorrowful sighing of the prisoners come before you; +
    according to the greatness of your power, preserve those who are condemned to die.

13  As for the blasphemy by which our neighbors have blasphemed you, +
    repay them, O Lord, seven-fold into their bosoms.

14  So we, who are your people and the sheep of your pasture, shall give you thanks for ever, +
    and will always be showing forth your praise from generation to generation.
