
Day V : Evening Prayer

Psalm 27 - Dominus illuminatio

1   The Lord is my light and my salvation; whom then shall I fear? +
    The Lord is the strength of my life; of whom then shall I be afraid?

2   When the wicked, even my enemies and my foes, came upon me to eat up my flesh, +
    they stumbled and fell.

3   Though a host were encamped against me, yet my heart would not be afraid, +
    and though war rose up against me, yet would I put my trust in him.

4   One thing have I desired of the Lord; one thing I seek: +
    that I may dwell in the house of the Lord all the days of my life,

5   To behold the fair beauty of the Lord, +
    and to seek him in his temple.

6   For in the time of trouble he shall hide me in his tabernacle; +
    indeed, in the secret place of his dwelling he shall hide me, and set me high upon a rock of stone.

7   And now he shall lift up my head +
    above my enemies round about me.

8   Therefore I will offer in his dwelling an oblation with great gladness; +
    I will sing and speak praises unto the Lord.

9   Hearken to my voice, O Lord, when I cry unto you; +
    have mercy upon me and hear me.

10  You speak to my heart and say, “Seek my face.” +
    Your face, O Lord, will I seek.

11  O hide not your face from me, +
    nor cast your servant away in displeasure.

12  You have been my helper; +
    leave me not, neither forsake me, O God of my salvation.

13  When my father and my mother forsake me, +
    the Lord takes me in.

14  Teach me your way, O Lord, +
    and lead me in the right way, because of my enemies.

15  Deliver me not over to the will of my adversaries, +
    for there are false witnesses who have risen up against me, and those who speak wrong.

16  I would utterly have fainted, +
    had I not believed that I would see the goodness of the Lord in the land of the living.

17  O wait for the Lord; be strong, and he shall comfort your heart. +
    O put your trust in the Lord.
