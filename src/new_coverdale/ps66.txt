
Psalm 66 — Jubilate Deo

1   Be joyful in God, all you lands; +
    sing praises to the honor of his Name; make his praise to be glorious.

2   Say to God, “How wonderful are your works; +
    through the greatness of your power shall your enemies cower before you.

3   For all the world shall worship you, +
    sing to you, and praise your Name.”

4   O come and see the works of God, +
    how wonderful he is in his doing toward all people.

5   He turned the sea into dry land, so that they went through the water on foot; +
    therefore in him let us rejoice.

6   He rules with his power for ever; his eyes keep watch over the nations; +
    let not the rebellious exalt themselves.

7   Bless our God, you peoples, +
    and make the voice of his praise to be heard,

8   Who holds our soul in life, +
    and does not allow our feet to slip.

9   For you, O God, have proved us; +
    you have tried us, as silver is tried.

10  You brought us into the snare +
    and laid trouble upon our backs.

11  You allowed men to ride over our heads; we went through fire and water; +
    but you brought us out into a place of plenty.

12  I will go into your house with burnt offerings +
    and will pay you my vows,

13  Even those which I promised with my lips +
    and spoke with my mouth when I was in trouble.

14  I will offer you burnt sacrifices of fattened beasts, with the incense of rams; +
    I will offer bullocks and goats.

15  Come here and listen, all you who fear God, +
    and I will tell you what he has done for me.

16  I called to him with my mouth, +
    and gave him praises with my tongue.

17  If I had inclined toward wickedness with my heart, +
    the Lord would not have heard me.

18  But God has heard me +
    and considered the voice of my prayer.

19  Blessed be God who has not refused my prayer, +
    nor turned his mercy from me.
