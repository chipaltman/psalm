
Psalm 17 - Exaudi, Domine

1   Hear what is right, O Lord; consider my complaint; +
    hearken to my prayer, which does not come from lying lips.

2   Let justice for me come forth from your presence, +
    and let your eyes look upon that which is right.

3   You have tested and visited my heart in the night season. +
    If you try me you shall find no wickedness in me; my mouth shall not offend.

4   As for the works of others, +
    because of the words of your lips, I have kept myself from the ways of the violent. 

5   Hold my steps firmly in your paths, +
    that my footsteps may not slip.

6   I have called upon you, O God, for you will hear me; +
    incline your ear to me and hearken to my words.

7   Show your marvelous loving-kindness, you that are the Savior of those who put their trust in you +
    from the ones who resist your right hand.

8   Keep me as the apple of your eye; +
    hide me under the shadow of your wings,

9   From the ungodly who assault me, +
    even from my enemies who encompass me to take away my soul.

10  They have closed their heart to pity, +
    and their mouth speaks proud things.

11  They lie waiting in my way on every side, +
    watching how they may cast me down to the ground,

12  Like a lion that is greedy for its prey, +
    and like a young lion lurking in secret places.

13  Rise up, O Lord, confront them and cast them down; +
    deliver my soul from the ungodly by your sword and by your hand,

14  From those, O Lord, from those whose portion in life is of the world, +
    whose bellies you fill with your hidden treasure.

15  They have children at their desire, +
    and leave the rest of their abundance for their little ones.

16  But as for me, I will behold your presence in righteousness; +
    and when I awake and see your likeness, I shall be satisfied.
