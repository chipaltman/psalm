
Psalm 26 - Judica me, Domine

1   Be my judge, O Lord, for I have walked innocently; +
    my trust has been in the Lord; therefore I shall not fall.

2   Test me, O Lord, and prove me; +
    examine my heart and my mind.

3   For your loving-kindness is ever before my eyes, +
    and I will walk in your truth.

4   I have not dwelt with evildoers, +
    neither will I have fellowship with the deceitful.

5   I have hated the company of the wicked, +
    and will not sit among the ungodly.

6   I will wash my hands in innocence, O Lord, +
    and so will I go to your altar,

7   That I may lift up the voice of thanksgiving +
    and tell of all your wondrous works.

8   Lord, I have loved the habitation of your house +
    and the place where your honor dwells.

9   O take not away my soul with the sinners, +
    nor my life with the bloodthirsty,

10  Whose hands are full of wickedness, +
    and their right hand full of bribes.

11  But as for me, I will walk innocently; +
    O deliver me, and be merciful unto me.

12  My foot stands firm; +
    I will praise the Lord in the congregations.
