
Psalm 115 - Non nobis, Domine

1   Not unto us, O Lord, not unto us, but unto your Name give the praise, +
    for your loving mercy and for your truth’s sake.

2   Why shall the nations say, +
    “Where now is their God?”

3   As for our God, he is in heaven; +
    he has done whatsoever pleased him.

4   Their idols are silver and gold, +
    even the work of human hands.

5   They have mouths, but they speak not; +
    eyes have they, but they see not.

6   They have ears, but they hear not; +
    noses have they, but they smell not.

7   They have hands, but they feel not; feet have they, but they walk not; +
    neither is there any sound in their throat.

8   Those who make them are like them, +
    and so are all who put their trust in them.

9   But you, O house of Israel, trust in the Lord; +
    he is their helper and defender.

10  You house of Aaron, put your trust in the Lord; +
    he is their helper and defender.

11  You who fear the Lord, put your trust in the Lord; +
    he is their helper and defender.

12  The Lord has been mindful of us, and he shall bless us; +
    he shall bless the house of Israel; he shall bless the house of Aaron;

13  He shall bless those who fear the Lord, +
    both small and great together.

14  The Lord shall increase you more and more, +
    you and your children after you.

15  You are the blessed of the Lord, +
    who has made heaven and earth.

16  The heavens are the Lord’s; +
    the earth has he given to the children of men.

17  The dead praise you not, O Lord, +
    neither all those who go down into silence.

18  But we will praise the Lord, +
    from this time forth for evermore. Praise the Lord.
