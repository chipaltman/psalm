
Day IXXX : Evening Prayer

Psalm 141 - Domine, clamavi

1   Lord, I call upon you; hasten unto me, +
    and hear my voice when I cry unto you.

2   Let my prayer be set forth in your sight as incense, +
    and let the lifting up of my hands be an evening sacrifice.

3   Set a watch, O Lord, before my mouth, +
    and keep the door of my lips.

4   O let not my heart be inclined to any evil thing; +
    let me not be occupied in ungodly works with those who work wickedness, lest I eat of such things as please them.

5   Rather, let the righteous smite me, +
    and in their loving-kindness reprove me.

6   But let not the oil of the unrighteous anoint my head; +
    while I live, I will pray against their wickedness.

7   Let their rulers be overthrown in stony places, +
    that they may hear my words, for they are sweet.

8   Let their bones lie scattered at the mouth of the grave, +
    as when the ploughman scatters the earth in furrows.

9   But my eyes look unto you, O Lord God; +
    in you is my refuge; O do not cast out my soul.

10  Keep me from the snare which they have laid for me, +
    and from the traps of the evildoers.

11  Let the ungodly fall into their own nets together, +
    and let me ever escape them.
