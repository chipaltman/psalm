
Psalm 7 - Domine, Deus meus

1   O Lord my God, in you have I put my trust; +
    save me from all those who persecute me, and deliver me,

2   Lest they devour me like a lion and tear me in pieces +
    while there is none to help.

3   O Lord my God, if I have done any such thing, +
    if there be any wickedness in my hands,

4   If I have repaid evil to him who has dealt with me as a friend, +
    or plundered him who without any cause is my enemy,

5   Then let my enemy pursue me and overtake me, +
    let him trample my life into the ground, and lay my honor in the dust.

6   Stand up, O Lord, in your wrath, and lift yourself up against the fury of my enemies; +
    rise up for me in the judgment that you have commanded.

7   Then shall the assembly of the peoples be gathered about you; +
    lift yourself up again, O Lord, O judge of all the nations.

8   Give sentence for me, O Lord, according to my righteousness, +
    and according to the innocence that is in me.

9   O let the wickedness of the ungodly come to an end, +
    but establish the just.

10  For the righteous God +
    tries the very hearts and minds.

11  God is my shield and my defense; +
    he preserves those who are true of heart.

12  God is a righteous judge, strong and patient; +
    and God is provoked every day.

13  If a man will not repent, God will whet his sword; +
    he will bend his bow, and make it ready.

14  He has prepared for him the instruments of death; +
    he makes his arrows shafts of fire.

15  Behold, the ungodly is in labor with mischief; +
    he has conceived wickedness and brought forth lies.

16  He has made a pit and dug it out, +
    but will himself fall into the trap that he made for others.

17  For his malice shall come upon his own head, +
    and his wickedness shall fall on his own scalp.

18  I will give thanks unto the Lord, according to his righteousness, +
    and I will praise the Name of the Lord Most High.
