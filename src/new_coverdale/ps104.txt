
Day XX : Evening Prayer

Psalm 104 - Benedic, anima mea

1   Praise the Lord, O my soul. +
    O Lord my God, you have become exceedingly glorious; you are clothed with majesty and honor.

2   You clothe yourself with light as with a garment, +
    and spread out the heavens like a curtain.

3   You lay the beams of your chambers in the waters, +
    and make the clouds your chariot, and walk upon the wings of the wind.

4   You make winds your messengers, +
    and flames of fire your ministers.

5   You laid the foundations of the earth, +
    that it never should move at any time.

6   You cover it with the deep as with a garment; +
    the waters stand above the hills.

7   At your rebuke they fled; +
    at the voice of your thunder they hastened away.

8   They went up as high as the hills, and down to the valleys beneath, +
    even to the place you had appointed for them.

9   You have set bounds for them which they shall not pass; +
    neither shall they again cover the earth.

10  You send the springs into the rivers, +
    which run among the hills.

11  All beasts of the field drink thereof, +
    and the wild donkeys quench their thirst.

12  Beside them shall the birds of the air have their habitation +
    and sing among the branches.

13  You water the hills from above; +
    the earth is filled with the fruit of your works.

14  You bring forth grass for the cattle, +
    and plants for the service of mankind,

15  That they may bring food out of the earth, and wine that makes glad the heart, +
    and oil to make a cheerful countenance, and bread to strengthen the heart. 

16  The trees of the Lord also are full of sap, +
    even the cedars of Lebanon which he planted,

17  Wherein the birds make their nests, +
    and the fir trees are a dwelling for the stork.

18  The high hills are a refuge for the wild goats, +
    and so are the stony cliffs for the rock badgers.

19  You appointed the moon to mark the seasons, +
    and the sun knows its going down.

20  You make darkness that it may be night, +
    in which all the beasts of the forest move.

21  The lions, roaring after their prey, +
    seek their meat from God.

22  The sun arises, and they go away together, +
    and lay themselves down in their dens.

23  Man goes forth to his work, +
    and to his labor until the evening.

24  O Lord, how manifold are your works; +
    in wisdom you made them all; the earth is full of your creatures.

25  So is the great and wide sea also, +
    in which are things creeping innumerable, creatures both small and great.

26  There go the ships, and there is that Leviathan, +
    whom you made to take its pleasure therein.

27  These all wait upon you, +
    that you may give them food in due season.

28  When you give it to them, they gather it, +
    and when you open your hand, they are filled with good things.

29  When you hide your face, they are troubled; +
    when you take away their breath, they die, and are turned again to their dust.

30  When you let your breath go forth, they shall be made, +
    and you shall renew the face of the earth.

31  The glorious majesty of the Lord shall endure for ever; +
    the Lord shall rejoice in his works.

32  He looks at the earth and it trembles; +
    if he even touches the hills, they shall smoke.

33  I will sing unto the Lord as long as I live; +
    I will praise my God while I have my being,

34  And so shall my words please him; +
    my joy shall be in the Lord.

35  As for sinners, they shall perish from the earth, and the ungodly shall come to an end. +
    Praise the Lord, O my soul. Praise the Lord.
