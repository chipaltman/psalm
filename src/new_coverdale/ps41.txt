
Day VIII : Evening Prayer

Psalm 41 - Beatus qui intelligit

1   Blessed is the one who considers the poor and needy; +
    the Lord shall deliver him in the time of trouble.

2   The Lord preserves him and keeps him alive, that he may be blessed upon earth, +
    and delivers him not over to the will of his enemies.

3   The Lord comforts him when he lies sick upon his bed, +
    and restores him from his bed of sickness.

4   I said, “Lord, be merciful to me; +
    heal my soul, for I have sinned against you.”

5   My enemies speak evil of me: +
    “When shall he die, and his name perish?”

6   And if anyone comes to see me, he speaks empty words; +
    his heart conceives falsehood within him, and when he goes forth, he tells it.

7   All my enemies whisper together against me; +
    even against me are they devising evil:

8   “A deadly thing has taken hold of him, +
    and now that he lies down, he will rise up no more.”

9   Indeed, even my own familiar friend, whom I trusted, who also ate of my bread, +
    has lifted up his heel against me.

10  But be merciful to me, O Lord; +
    raise me up again, and I shall repay them.

11  By this I know you favor me, +
    that my enemy does not triumph over me.

12  And when I am in health, you uphold me, +
    and shall set me before your face for ever.

13  Blessed be the Lord God of Israel, +
    world without end. Amen.
