
Day XXVIII : Evening Prayer

Psalm 136 - Confitemini

1   O give thanks unto the Lord, for he is gracious, +
    for his mercy endures for ever.

2   O give thanks unto the God of all gods, +
    for his mercy endures for ever.

3   O give thanks unto the Lord of all lords, +
    for his mercy endures for ever.

4   Who alone does great wonders, +
    for his mercy endures for ever.

5   Who by his excellent wisdom made the heavens, +
    for his mercy endures for ever.

6   Who laid out the earth above the waters, +
    for his mercy endures for ever.

7   Who made the great lights, +
    for his mercy endures for ever;

8   The sun to rule the day, +
    for his mercy endures for ever;

9   The moon and the stars to govern the night, +
    for his mercy endures for ever.

10  Who struck down the firstborn of Egypt, +
    for his mercy endures for ever;

11  And brought out Israel from among them, +
    for his mercy endures for ever;

12  With a mighty hand and an outstretched arm, +
    for his mercy endures for ever.

13  Who divided the Red Sea in two parts, +
    for his mercy endures for ever;

14  And made Israel to pass through the midst of it, +
    for his mercy endures for ever;

15  But as for Pharaoh and his host, he overthrew them in the Red Sea, +
    for his mercy endures for ever.

16  Who led his people through the wilderness, +
    for his mercy endures for ever.

17  Who smote great kings, +
    for his mercy endures for ever;

18  And slew mighty kings, +
    for his mercy endures for ever;

19  Sihon king of the Amorites, +
    for his mercy endures for ever;

20  And Og the king of Bashan, +
    for his mercy endures for ever;

21  And gave away their land for an inheritance, +
    for his mercy endures for ever;

22  Even for an inheritance for Israel his servant, +
    for his mercy endures for ever.

23  Who remembered us when we were in trouble, +
    for his mercy endures for ever;

24  And delivered us from our enemies, +
    for his mercy endures for ever.

25  Who gives food to all flesh, +
    for his mercy endures for ever.

26  O give thanks unto the God of heaven, +
    for his mercy endures for ever.

27  O give thanks unto the Lord of lords +
    for his mercy endures for ever.
