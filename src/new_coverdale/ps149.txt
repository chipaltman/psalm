
Psalm 149 - Cantate Domino

1   Praise the Lord. O sing unto the Lord a new song; +
    let the congregation of the faithful praise him.

2   Let Israel rejoice in the one who made him, +
    and let the children of Zion be joyful in their King.

3   Let them praise his Name in the dance; +
    let them sing praises unto him with timbrel and harp.

4   For the Lord has pleasure in his people +
    and gives victory to those who are oppressed.

5   Let the faithful be joyful with glory; +
    let them rejoice upon their beds.

6   Let the praises of God be in their mouth +
    and a two-edged sword in their hands,

7   To inflict vengeance on the nations, +
    and to rebuke the peoples,

8   To bind their kings in chains, +
    and their nobles with links of iron,

9   That they may execute judgment upon them, as it is written; +
    this is the honor of all his servants. Praise the Lord.
