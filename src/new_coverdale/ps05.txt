
Psalm 5 - Verba mea auribus

1   Give ear to my words, O Lord; +
    consider my meditation.

2   O hearken unto the voice of my calling, my King and my God, +
    for unto you will I make my prayer.

3   My voice shall you hear in the morning, O Lord; +
    early in the morning will I direct my prayer unto you, and will look up.

4   For you are not a god who has pleasure in wickedness, +
    neither shall any evil dwell with you.

5   The boastful shall not stand in your sight, +
    for you hate all those who work iniquity.

6   You shall destroy those who speak lies; +
    the Lord will abhor the bloodthirsty and the deceitful.

7   But as for me, through the multitude of your mercies I will come into your house, +
    and in reverence will I bow myself toward your holy temple.

8   Lead me, O Lord, in your righteousness, because of my enemies; +
    make your way straight before my face.

9   For there is no faithfulness in their mouth; +
    their heart is eaten up with wickedness.

10  Their throat is an open sepulcher; +
    they flatter with their tongue.

11  Declare them guilty, O God; let them fall because of their own devices; +
    because of the multitude of their transgressions cast them out, for they have rebelled against you.

12  But let all those who put their trust in you rejoice; +
    let them ever give thanks because you defend them; those who love your Name shall be joyful in you.

13  For you, Lord, will give your blessing unto the righteous, +
    and with your favorable kindness you will defend him as with a shield.
