
Psalm 2 - Quare fremuerunt gentes?

1   Why do the nations so furiously rage together? +
    And why do the peoples devise a vain thing?

2   The kings of the earth stand up, and the rulers take counsel together, +
    against the Lord and against his Anointed:

3   “Let us break their bonds asunder +
    and cast away their cords from us.”

4   He who dwells in heaven shall laugh them to scorn; +
    the Lord shall hold them in derision.

5   Then shall he speak to them in his wrath +
    and terrify them in his great anger:

6   “I myself have set my King +
    upon my holy hill of Zion.”

7   I will proclaim the decree of the Lord; +
    he said to me, “You are my Son; this day have I begotten you.

8   Ask of me, and I shall give you the nations for your inheritance +
    and the ends of the earth for your possession.

9   You shall bruise them with a rod of iron +
    and break them in pieces like a potter’s vessel.”

10  Be wise now, O you kings; +
    be warned, you judges of the earth.

11  Serve the Lord in fear, +
    and rejoice with trembling. 

12  Kiss the Son, lest he be angry, and you perish in the way; for his wrath is quickly kindled. +
    Blessed are all those who put their trust in him.
