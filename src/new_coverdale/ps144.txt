
Day XXX : Morning Prayer

Psalm 144 - Benedictus Dominus

1   Blessed be the Lord my strength, +
    who teaches my hands to war and my fingers to fight,

2   My hope and my fortress, my stronghold and deliverer, my defender in whom I trust, +
    who subdues the peoples under me.

3   O Lord, what is man, that you have shown such respect to him, +
    or the son of man, that you so regard him?

4   Man is like a thing of nought; +
    his time passes away like a shadow.

5   Bow your heavens, O Lord, and come down; +
    touch the mountains, and they shall smoke.

6   Cast forth your lightning and scatter them; +
    shoot out your arrows and consume them.

7   Send down your hand from above; +
    deliver me, and take me out of the great waters, from the hand of strangers,

8   Whose mouth talks of vain things, +
    and whose right hand is a right hand of falsehood.

9   I will sing a new song unto you, O God, +
    and sing praises unto you upon a ten-stringed lute.

10  You have given victory to kings, +
    and have delivered David your servant from the peril of the sword.

11  Save me, and deliver me from the hand of strangers, +
    whose mouth talks of vain things, and whose right hand is a right hand of falsehood;

12  That our sons may grow up as young plants, +
    and that our daughters may be as the polished corners of the temple,

13  That our storehouses may be full and plenteous with all manner of grain, +
    that our sheep may bring forth thousands and ten thousands in our fields,

14  That our oxen may be strong to labor, that there be no decay, +
    no leading into captivity, and no outcry in our streets.

15  Happy are the people of whom this is so; +
    indeed, blessed are the people who have the Lord for their God.
