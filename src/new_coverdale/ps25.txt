
Psalm 25 - Ad te, Domine, levavi

1   Unto you, O Lord, will I lift up my soul; my God, I have put my trust in you; +
    O let me not be ashamed, neither let my enemies triumph over me.

2   For all those who hope in you shall not be ashamed, +
    but those who deal untruly shall be put to confusion.

3   Show me your ways, O Lord, +
    and teach me your paths.

4   Lead me forth in your truth and teach me, +
    for you are the God of my salvation; in you has been my hope all the day long.

5   Call to remembrance, O Lord, your tender mercies, +
    and your loving-kindnesses, which have been from of old.

6   O remember not the sins and offenses of my youth, +
    but according to your mercy think on me, O Lord, in your goodness.

7   Gracious and righteous is the Lord; +
    therefore will he teach sinners in the way.

8   Those who are meek shall he guide in judgment, +
    and those who are gentle shall he teach his way.

9   All the paths of the Lord are mercy and truth +
    to those who keep his covenant and his testimonies.

10  For your Name’s sake, O Lord, +
    forgive my sin, for it is great.

11  Who is the one who fears the Lord? +
    He shall teach him in the way that he shall choose.

12  He shall dwell at ease, +
    and his seed shall inherit the land.

13  The Lord reveals his secret counsel to those who fear him, +
    and he will show them his covenant.

14  My eyes are ever looking to the Lord, +
    for he shall pluck my feet out of the net.

15  Turn to me, and have mercy on me, +
    for I am desolate and in misery.

16  The sorrows of my heart are enlarged; +
    O bring me out of my troubles.

17  Look upon my adversity and misery +
    and forgive me all my sin.

18  Consider my enemies, how many they are, +
    and how they bear a tyrannous hate against me.

19  O keep my soul and deliver me; +
    let me not be ashamed, for I have put my trust in you.

20  Let integrity and righteous dealing preserve me, +
    for my hope has been in you.

21  Deliver Israel, O God, +
    out of all his troubles.
