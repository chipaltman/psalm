
Psalm 127 - Nisi Dominus

1   Unless the Lord builds the house, +
    their labor is in vain who build it.

2   Unless the Lord keeps the city, +
    the watchman keeps vigil in vain.

3   It is in vain that you rise up early, and take rest so late, and eat the bread of toil, +
    for he gives to his beloved sleep.

4   Behold, children are a heritage from the Lord, +
    and the fruit of the womb is a gift that comes from him.

5   Like arrows in the hand of a warrior, +
    so are the children of one’s youth.

6   Happy is the man who has his quiver full of them; +
    he shall not be ashamed when he speaks with his enemies in the gate.
