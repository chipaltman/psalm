
Psalm 134 - Ecce nunc

1   Behold now, praise the Lord, +
    all you servants of the Lord,

2   You that stand by night in the house of the Lord, +
    even in the courts of the house of our God.

3   Lift up your hands in the sanctuary +
    and sing praises unto the Lord.

4   The Lord who made heaven and earth +
    give you blessing out of Zion.
