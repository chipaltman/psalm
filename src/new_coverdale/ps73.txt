
Day XIV : Evening Prayer

Psalm 73 - Quam bonus Isræl!

1   Truly, God is loving to Israel, +
    even to those who have a clean heart.

2   Nevertheless, my feet had almost stumbled; +
    my steps had nearly slipped.

3   For I was envious of the proud +
    when I saw the ungodly in such prosperity.

4   For they are in no fear of death, +
    but their bodies are healthy and strong.

5   They come to no misfortune like other folk, +
    neither are they afflicted like others.

6   Therefore they wear pride as a necklace, +
    and they wrap themselves with violence as with a garment.

7   Their eyes swell with greed, +
    and their hearts overflow with wicked thoughts.

8   Their talk is malice and mockery; +
    they have proud looks, and tyrannous words are on their lips.

9   For they set their mouth against the heavens, +
    and their evil speech spreads through the world.

10  Therefore the people turn to them, +
    and find no fault in them.

11  They say, “How should God perceive it? +
    Is there knowledge in the Most High?”

12  Behold, these are the ungodly; +
    they prosper in their ways, and they have riches in possession. 

13  I said, “Surely in vain have I cleansed my heart +
    and washed my hands in innocence.”

14  All the day long have I been afflicted, +
    and chastened every morning.

15  Indeed, had I spoken as they do, +
    then would I have betrayed the generation of your children.

16  When I sought to understand this, +
    it was too hard for me,

17  Until I went into the sanctuary of God; +
    then I understood their end.

18  Surely, you set them in slippery places, +
    and cast them down, and destroy them.

19  Oh, how suddenly are they consumed; +
    they perish and come to a fearful end.

20  Indeed, like a dream when one awakens, +
    so shall you make their image vanish out of the city.

21  Thus my heart was grieved, +
    and I was wounded within;

22  So foolish was I, and ignorant, +
    as if I were a beast before you.

23  Nevertheless, I am always with you, +
    for you hold me by my right hand.

24  You shall guide me with your counsel, +
    and after that receive me with glory.

25  Whom have I in heaven but you? +
    And there is no one on earth whom I desire in comparison with you.

26  Though my flesh and my heart fail me, +
    God is the strength of my heart, and my portion for ever.

27  For behold, those who forsake you shall perish; +
    you destroy all those who are unfaithful to you.

28  But it is good for me to hold fast to God, to put my trust in the Lord God, +
    and to speak of all your works in the gates of the city of Zion.
