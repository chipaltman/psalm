
Psalm 101 - Misericordiam et judicium

1   My song shall be of mercy and judgment; +
    unto you, O Lord, will I sing.

2   O let me have understanding +
    in the way of godliness.

3   When will you come to me? +
    I will walk in my house with integrity of heart.

4   I will set no wicked thing before my eyes; + 
    I hate the sins of unfaithfulness; no such thing shall cleave to me.

5   A crooked heart shall depart from me; +
    I will not know a wicked person.

6   Whoever secretly slanders his neighbor, +
    him will I destroy.

7   Whoever has a proud look and an arrogant heart, +
    I will not suffer him.

8   My eyes shall look with favor upon the faithful in the land, +
    that they may dwell with me.

9   Whoever leads a godly life, +
    he shall be my servant.

10  No deceitful person shall dwell in my house; +
    the one who tells lies shall not tarry in my sight.

11  I shall soon destroy all the ungodly who are in the land, +
    that I may root out all evildoers from the city of the Lord.
