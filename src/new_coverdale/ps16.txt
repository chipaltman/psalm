
Psalm 16 - Conserva me, Domine

1   Preserve me, O God, +
    for in you have I put my trust.

2   O my soul, you have said unto the Lord, +
    “You are my Lord; I have no good apart from you.”

3   All my delight is upon the saints who are on the earth, +
    and upon those who excel in virtue.

4   But those who run after another god +
    shall have great trouble.

5   Their drink offerings of blood I will not offer, +
    neither make mention of their names with my lips.

6   The Lord himself is the portion of my inheritance and of my cup; +
    you shall maintain my lot.

7   The boundaries have fallen for me in pleasant places; +
    indeed, I have a goodly heritage.

8   I will thank the Lord for giving me counsel; +
    my heart also chastens me in the night season.

9   I have set the Lord always before me; +
    he is at my right hand; therefore I shall not fall.

10  Therefore my heart is glad and my soul rejoices. +
    My flesh also shall rest in hope.

11  For you shall not leave my soul in the grave, +
    neither shall you allow your Holy One to see corruption.

12  You shall show me the path of life; in your presence is the fullness of joy, +
    and at your right hand there is pleasure for evermore.
