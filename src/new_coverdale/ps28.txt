
Psalm 28 - Ad te, Domine

1   Unto you will I cry, O Lord my rock; do not be deaf to my prayer; +
    lest, if you do not answer, I become like those who go down into the pit.

2   Hear the voice of my humble petitions when I cry unto you, +
    when I hold up my hands toward the sanctuary of your holy temple.

3   O cast me not away, neither destroy me with the ungodly and evildoers, +
    who speak as friends to their neighbors, but imagine evil in their hearts.

4   Reward them according to their deeds +
    and according to the wickedness of their own inventions.

5   Recompense them according to the work of their hands; +
    pay them what they have deserved.

6   For they regard not in their mind the works of the Lord, nor the operation of his hands; +
    therefore he shall break them down and not build them up.

7   Praised be the Lord, +
    for he has heard the voice of my humble petitions.

8   The Lord is my strength and my shield; my heart has trusted in him, and I am helped; +
    therefore my heart dances for joy, and in my song will I praise him.

9   The Lord is my strength, +
    and he is the sure defense of his Anointed.

10  O save your people, and give your blessing to your inheritance;  +
    feed them, and lift them up for ever.
