
Day IXX : Evening Prayer

Psalm 98 - Cantate Domino

1   O sing unto the Lord a new song, +
    for he has done marvelous things.

2   With his own right hand and with his holy arm, +
    he has won for himself the victory.

3   The Lord declared his salvation; +
    his righteousness has he openly shown in the sight of the nations.

4   He has remembered his mercy and truth toward the house of Israel, +
    and all the ends of the world have seen the salvation of our God.

5   Show yourselves joyful in the Lord, all you lands; +
    sing, rejoice, and give thanks.

6   Praise the Lord with the harp; +
    sing with the harp a psalm of thanksgiving.

7   With trumpets also and horns, +
    O show yourselves joyful before the Lord, the King.

8   Let the sea make a noise, and all that is in it, +
    the round world, and those who dwell therein.

9   Let the rivers clap their hands, and let the hills be joyful together before the Lord, +
    for he has come to judge the earth.

10  With righteousness shall he judge the world, +
    and the peoples with equity.
