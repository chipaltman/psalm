
Day XXX : Evening Prayer

Psalm 147 - Laudate Dominum

1   Praise the Lord, for it is a good thing to sing praises unto our God; +
    indeed, a joyful and pleasant thing it is to be thankful.

2   The Lord builds up Jerusalem, +
    and gathers together the outcasts of Israel.

3   He heals those who are broken in heart, +
    and binds up their wounds.

4   He counts the number of the stars, +
    and calls them all by their names.

5   Great is our Lord, and great is his power; +
    indeed, his wisdom is infinite.

6   The Lord lifts up the meek, +
    and brings the ungodly down to the ground.

7   O sing unto the Lord with thanksgiving; +
    sing praises with the harp unto our God,

8   Who covers the heavens with clouds, and prepares rain for the earth, +
    and makes the grass to grow upon the mountains, and plants for the use of men,

9   Who gives food to the cattle +
    and feeds the young ravens that call upon him.

10  He has no pleasure in the strength of a horse; +
    neither does he delight in any man’s strength.

11  But the Lord’s delight is in those who fear him +
    and put their trust in his mercy.

12  Praise the Lord, O Jerusalem; +
    praise your God, O Zion.

13  For he has made strong the bars of your gates +
    and has blessed your children within you.

14  He makes peace in your borders +
    and fills you with the finest of wheat.

15  He sends forth his commandment upon the earth, +
    and his word runs very swiftly.

16  He gives snow like wool +
    and scatters the white frost like ashes.

17  He casts forth his ice like crumbs; +
    who is able to abide his frost?

18  He sends out his word and melts them; +
    he blows with his wind, and the waters flow.

19  He declares his word unto Jacob, +
    his statutes and ordinances unto Israel.

20  He has not dealt so with other nations; +
    neither have they knowledge of his laws. Praise the Lord.
