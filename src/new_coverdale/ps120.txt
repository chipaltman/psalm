
Day XXVII : Morning Prayer

Psalm 120 - Ad Dominum

1   When I was in trouble I called upon the Lord, +
    and he hearkened to my calling.

2   Deliver my soul, O Lord, from lying lips +
    and from a deceitful tongue.

3   What reward shall be given or done unto you, O false tongue? +
    Even mighty and sharp arrows, with hot burning coals.

4   Woe is me, that I am constrained to dwell in Meshech, +
    and to have my habitation among the tents of Kedar.

5   My soul has long dwelt among those +
    who are enemies of peace.

6   I labor for peace, but when I speak to them of it, +
    they make themselves ready for battle.
