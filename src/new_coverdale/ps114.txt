
Day XXIII : Evening Prayer

Psalm 114 - In exitu Israel

1   When Israel came out of Egypt, +
    and the house of Jacob from among a people of a foreign tongue,

2   Judah was God’s sanctuary, +
    and Israel his dominion. 

3   The sea beheld it and fled; +
    Jordan was driven back. 

4   The mountains skipped like rams, +
    and the little hills like young sheep. 

5   What ailed you, O sea, that you fled? +
    O Jordan, that you were driven back?

6   You mountains, that you skipped like rams, +
    and you little hills like young sheep?

7   Tremble, O earth, at the presence of the Lord, +
    at the presence of the God of Jacob,

8   Who turned the hard rock into a pool of water, +
    and the flint stone into a springing well.
