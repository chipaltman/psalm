
Psalm 103 - Benedic, anima mea

1   Praise the Lord, O my soul, +
    and all that is within me, praise his holy Name.

2   Praise the Lord, O my soul, +
    and forget not all his benefits:

3   Who forgives all your sin +
    and heals all your infirmities,

4   Who saves your life from the pit +
    and crowns you with mercy and loving-kindness,

5   Who satisfies you with good things, +
    renewing your youth like an eagle’s.

6   The Lord executes righteousness and judgment +
    for all those who are oppressed with wrong.

7   He showed his ways to Moses, +
    his works to the children of Israel.

8   The Lord is full of compassion and mercy, +
    long-suffering and of great goodness.

9   He will not always chide us, +
    neither will he keep his anger for ever.

10  He has not dealt with us according to our sins, +
    nor rewarded us according to our wickedness.

11  For as the heavens are high above the earth, +
    so great is his mercy also toward those who fear him.

12  As far as the east is from the west, +
    so far has he set our sins from us.

13  As a father pities his own children, +
    so is the Lord merciful to those who fear him.

14  For he knows whereof we are made; +
    he remembers that we are but dust.

15  The days of man are as grass; +
    he flourishes as a flower of the field.

16  For as soon as the wind goes over it, it is gone, +
    and its place shall know it no more.

17  But the merciful goodness of the Lord endures for ever and ever upon those who fear him, +
    and his righteousness upon children’s children,

18  Even upon those who keep his covenant +
    and think upon his commandments to do them.

19  The Lord has prepared his throne in heaven, +
    and his kingdom rules over all.

20  O praise the Lord, you angels of his, you that excel in strength, +
    you that fulfill his commandment, and hearken unto the voice of his words.

21  O praise the Lord, all you his hosts, +
    you servants of his that do his pleasure.

22  O speak good of the Lord, all you works of his, in all places of his dominion; +
    praise the Lord, O my soul.
