
Psalm 4 - Cum invocarem

1   Hear me when I call, O God of my righteousness; +
    you set me free when I was in trouble; have mercy upon me, and hear my prayer.

2   O you children of men, how long will you blaspheme my honor, +
    and have such pleasure in vanity, and seek after falsehood?

3   Know this also, that the Lord has chosen for himself the one that is godly; +
    when I call upon the Lord, he will hear me.

4   Stand in awe, and sin not; +
    commune with your own heart upon your bed, and be still.

5   Offer the sacrifice of righteousness +
    and put your trust in the Lord.

6   There are many that say, “Who will show us any good?” +
    Lord, lift up the light of your countenance upon us.

7   You have put gladness in my heart, +
    more than when others’ grain and wine and oil increased.

8   I will lay me down in peace, and take my rest; +
    for you, Lord, only, make me dwell in safety.
