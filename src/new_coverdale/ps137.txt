
Psalm 137 - Super flumina

1   By the waters of Babylon we sat down and wept, +
    when we remembered you, O Zion.

2   As for our harps, we hung them up +
    upon the trees that are therein.

3   For those who led us away captive required of us a song and melody in our heaviness: +
    “Sing us one of the songs of Zion.”

4   How shall we sing the Lord’s song +
    in the land of our captivity?

5   If I forget you, O Jerusalem, +
    let my right hand forget its skill.

6   If I do not remember you, let my tongue cleave to the roof of my mouth, +
    if I prefer not Jerusalem above my dearest joy.

7   Remember the children of Edom, O Lord, in the day of Jerusalem, +
    how they said, “Down with it, down with it, even to the ground.”

8   O daughter of Babylon, wasted with misery, +
    happy shall be the one who rewards you as you have done to us.

9   Blessed shall he be who takes your children +
    and throws them against the stones.
