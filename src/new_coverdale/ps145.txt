
Psalm 145 - Exaltabo te, Deus

1   I will magnify you, O God my King, +
    and I will praise your Name for ever and ever.

2   Every day will I give thanks to you, +
    and praise your Name for ever and ever.

3   Great is the Lord, and most worthy to be praised; +
    there is no end of his greatness.

4   One generation shall praise your works to another, +
    and shall declare your power.

5   As for me, I will be talking of the glorious splendor of your majesty, +
    and of all your wondrous works.

6   They shall speak of the might of your marvelous acts, +
    and I also will tell of your greatness.

7   The remembrance of your abundant goodness shall they proclaim, +
    and they shall sing of your righteousness.

8   The Lord is gracious and merciful, +
    long-suffering, and of great kindness.

9   The Lord is loving to everyone, +
    and his mercy is over all his works.

10  All your works praise you, O Lord, +
    and your faithful servants give thanks to you.

11  They speak of the glory of your kingdom +
    and talk of your power,

12  That your power may be known to the children of men, +
    even the glorious splendor of your kingdom.

13  Your kingdom is an everlasting kingdom, +
    and your dominion endures throughout all ages.

14  The Lord upholds all those who fall +
    and lifts up all those who are bowed down.

15  The eyes of all wait upon you, O Lord, +
    and you give them their food in due season.

16  You open wide your hand, +
    and fill all things living with plenteousness.

17  The Lord is righteous in all his ways +
    and merciful in all his works.

18  The Lord is near to all those who call upon him, +
    to all who call upon him faithfully.

19  He will fulfill the desire of those who fear him; +
    he also will hear their cry and will help them.

20  The Lord preserves all those who love him, +
    but he will destroy all the ungodly.

21  My mouth shall speak the praise of the Lord; +
    and let all flesh give thanks unto his holy Name for ever and ever.
