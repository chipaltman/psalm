
Psalm 39  - Dixi, Custodiam
    
1   I said, “I will take heed to my ways, +
    that I may not offend with my tongue.

2   I will keep my mouth as with a bridle +
    while the ungodly are in my sight.”

3   I held my tongue and spoke nothing; +
    I kept silence, even from good words; but it was pain and grief to me.

4   My heart was hot within me, and while I was thus pondering the fire kindled, +
    and at the last I spoke with my tongue:

5   “Lord, let me know my end and the number of my days, +
    that I may learn how short my life is.

6   Behold, you have made my days as a span in length, +
    and my age is even as nothing before you; and truly, everyone living is but a breath.

7   For everyone walks about as a shadow, and disquiets himself in vain; +
    he heaps up riches and cannot tell who shall gather them.

8   And now, Lord, what is my hope? +
    Truly, my hope is in you.

9   Deliver me from all my offenses, +
    and make me not a taunt of the foolish.

10  I became mute and opened not my mouth, +
    for it was you that brought it to pass.

11  Take your affliction from me; +
    I am consumed by the blows of your heavy hand.

12  When you, with rebukes, chasten someone for sin, you consume what is dear to him, like a moth eating a garment; +
    everyone therefore is but vanity.

13  Hear my prayer, O Lord, and with your ears consider my cry; +
    hold not your peace at my tears.

14  For I am a stranger with you, +
    and a sojourner, as all my fathers were.

15  O turn your gaze from me, that I may again be glad, +
    before I go away to be seen no more.”
