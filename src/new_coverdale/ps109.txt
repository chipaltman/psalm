
Psalm 109 - Deus, laudem

1   Hold not your tongue, O God of my praise, +
    for the mouth of the ungodly, the mouth of the deceitful is opened upon me.

2   They have spoken against me with false tongues; +
    they encompassed me with words of hatred, and fought against me without a cause.

3   In return for the love that I had for them, they have become my adversaries; +
    but I give myself to prayer.

4   Thus have they rewarded me evil for good, +
    and hatred for my love.

5   Set an ungodly man to be ruler over him, +
    and let an accuser stand at his right hand.

6   When judgment is given, let him be condemned, +
    and let sentence be passed on him for guilt.

7   Let his days be few, +
    and let another take his office.

8   Let his children be fatherless +
    and his wife a widow.

9   Let his children be vagabonds and beg their bread; +
    let them be driven out, even from desolate places.

10  Let the creditor consume all that he has, +
    and let strangers take his labor for spoil.

11  Let there be no one to pity him, +
    nor to have compassion upon his fatherless children.

12  Let his posterity be destroyed, +
    and in the next generation let his name be blotted out.

13  Let the wickedness of his fathers be held in remembrance in the sight of the Lord, +
    and let not the sin of his mother be blotted out.

14  Let them be always before the Lord, +
    that he may root out the memorial of them from the earth, 

15  Because he was not minded to do good, +
    but persecuted to death the poor and needy, and those who were brokenhearted.

16  His delight was in cursing; let curses come upon him; +
    he loved not blessing; therefore let it be far from him.

17  He clothed himself with cursing as with a garment, +
    so let it soak into his body like water, and like oil into his bones.

18  Let it be to him as the cloak that he has on +
    and as the belt that he always wears.

19  Let this be the recompense from the Lord to my enemies, +
    and to those who speak evil against my soul.

20  But deal with me, O Lord God, according to your Name; +
    for sweet is your mercy. 

21  O deliver me, for I am helpless and poor, +
    and my heart is wounded within me.

22  I disappear like the shadow that lengthens, +
    and am shaken off like a grasshopper.

23  My knees are weak through fasting; +
    my flesh is grown lean for want of nourishment.

24  I have become a reproach to them; +
    when they look on me, they shake their heads.

25  Help me, O Lord my God; +
    save me according to your mercy;

26  And they shall know that this is your hand, +
    and that you, O Lord, have done it.

27  Though they curse, yet you bless; +
    let them be confounded who rise up against me, but let your servant rejoice.

28  Let my adversaries be clothed with shame, +
    and let them cover themselves with their own disgrace as with a cloak.

29  As for me, I will give great thanks unto the Lord with my mouth, +
    and praise him among the multitude,

30  For he shall stand at the right hand of the poor, +
    to save their souls from the unrighteous judges.
