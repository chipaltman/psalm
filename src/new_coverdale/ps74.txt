
Psalm 74 - Ut quid, Deus?

1   O God, why have you utterly cast us off ? +
    Why is your wrath so hot against the sheep of your pasture?

2   O think upon your congregation +
    whom you have purchased and redeemed of old.

3   Think upon the tribe of your inheritance, +
    and Mount Zion, where you have dwelt.

4   Draw near and behold how all is made desolate +
    and how the enemy has destroyed all that is in your sanctuary.

5   Your adversaries roar in the midst of your holy place +
    and set up their banners as tokens of victory.

6   Like hewers of timber in a thicket of trees, +
    so have they broken down all the carved work with axes and hammers.

7   They have set fire to your holy place +
    and have defiled the dwelling place of your Name, even to the ground.

8   They said in their hearts, “Let us make havoc of them altogether.” +
    Thus have they burnt up all the houses of God in the land.

9   We do not see any signs; there is not one prophet anymore; +
    there is no one who knows how long these things shall continue. 

10  O God, how long shall the adversary do this dishonor? +
    How long shall the enemy blaspheme your Name, for ever?

11  Why do you withdraw your hand? +
    Why do you not take your right hand out of your bosom to consume the enemy?

12  For God is my King of old; +
    he is the one bringing help upon the earth.

13  You divided the sea through your power; +
    you broke the heads of the dragons in the waters.

14  You smote the heads of Leviathan in pieces +
    and gave him to be food for the people in the wilderness.

15  You brought fountains and waters out of the hard rocks, +
    and you dried up mighty waters.

16  The day is yours, and the night is yours; +
    you have prepared the light and the sun.

17  You have set all the borders of the earth; +
    you have made summer and winter.

18  Remember this, O Lord, how the enemy scoffed, +
    and how the foolish people have blasphemed your Name.

19  Deliver not the soul of your turtledove to the wild beasts, +
    and forget not for ever the lives of your poor.

20  Look upon your covenant, +
    for the dark places of the earth are full of violence.

21  Let not the oppressed go away ashamed, +
    but let the poor and needy give praise to your Name.

22  Arise, O God, maintain your own cause; +
    remember how the foolish one blasphemes you daily.

23  Forget not the voice of your enemies, +
    nor the tumult of those who hate you, which increases ever more and more.
