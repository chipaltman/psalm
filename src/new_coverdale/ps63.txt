
Psalm 63 — Deus, Deus meus

1   O God, you are my God; +
    early will I seek you.

2   My soul thirsts for you, my flesh also longs after you, +
    in a barren and dry land where there is no water.

3   Thus I have looked upon you in your holy place, +
    that I might behold your power and glory.

4   For your loving-kindness is better than life itself; +
    my lips shall praise you.

5   As long as I live I will magnify you, +
    and lift up my hands in your Name.

6   My soul shall be satisfied, as with marrow and fatness, +
    when my mouth praises you with joyful lips.

7   When I remember you on my bed, +
    I meditate on you in the watches of the night. 

8   Because you have been my helper, +
    therefore under the shadow of your wings I will rejoice.

9   My soul clings to you; +
    your right hand has upheld me.

10  Those who seek to destroy my life +
    shall go down into the earth.

11  Let them fall upon the edge of the sword, +
    that they may be a portion for jackals.

12  But the King shall rejoice in God; all those who swear by him shall be commended, +
    for the mouth of those who speak lies shall be stopped.
