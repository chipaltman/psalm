//! # The 30-Day Cycle of Psalms
//!
//! Read the Psalms according to the traditional 30-day cycle.  
//!
//! **SPOILER ALERT:** It is the whole thing in a month *(i.e. hard mode, a slog).*
//!
//! ```mono
//! psalm v0.0.#
//!
//! USAGE:
//!     psalm | less
//!
//! ```
//! # Example
//!
//! On the fifth day of each month:
//! ```mono
//! Day V : Morning Prayer
//!     
//! Psalm 24 - Domini est terra
//!     
//! 
//! 1   The earth is the Lord’s and all that is in it, +
//!     the compass of the world and those who dwell therein.
//!     
//!         --snikt!--
//!
//! Day V : Evening Prayer
//!     
//! Psalm 27 - Dominus illuminatio
//!     
//! 1   The Lord is my light and my salvation; whom then shall I fear? +
//!     The Lord is the strength of my life; of whom then shall I be afraid?
//! ```
//!
//! Psalm text taken from the *New Coverdale Psalter, The Book of Common Prayer (2019).*  
//! *Copyright © 2019 by the Anglican Church in North America*
//!

use chrono::prelude::*;
use std::collections::HashMap;


fn main() {
    let today = Local::now().day();
    let proper = get_proper(today);
    print_new_coverdale(proper);
}

fn get_proper(day: u32) -> Vec<u32> {
    let mut day_to_proper = HashMap::new();

    day_to_proper.insert(1, vec![1, 2, 3, 4, 5, 6, 7, 8]);
    day_to_proper.insert(2, vec![9, 10, 11, 12, 13, 14]);
    day_to_proper.insert(3, vec![15, 16, 17, 18]);
    day_to_proper.insert(4, vec![19, 20, 21, 22, 23]);
    day_to_proper.insert(5, vec![24, 25, 26, 27, 28, 29]);
    day_to_proper.insert(6, vec![30, 31, 32, 33, 34]);
    day_to_proper.insert(7, vec![35, 36, 37]);
    day_to_proper.insert(8, vec![38, 39, 40, 41, 42, 43]);
    day_to_proper.insert(9, vec![44, 45, 46, 47, 48, 49]);
    day_to_proper.insert(10, vec![50, 51, 52, 53, 54, 55]);
    day_to_proper.insert(11, vec![56, 57, 58, 59, 60, 61]);
    day_to_proper.insert(12, vec![62, 63, 64, 65, 66, 67]);
    day_to_proper.insert(13, vec![68, 69, 70]);
    day_to_proper.insert(14, vec![71, 72, 73, 74]);
    day_to_proper.insert(15, vec![75, 76, 77, 78]);
    day_to_proper.insert(16, vec![79, 80, 81, 82, 83, 84, 85]);
    day_to_proper.insert(17, vec![86, 87, 88, 89]);
    day_to_proper.insert(18, vec![90, 91, 92, 93, 94]);
    day_to_proper.insert(19, vec![95, 96, 97, 98, 99, 100, 101]);
    day_to_proper.insert(20, vec![102, 103, 104]);
    day_to_proper.insert(21, vec![105, 106]);
    day_to_proper.insert(22, vec![107, 108, 109]);
    day_to_proper.insert(23, vec![110, 111, 112, 113, 114, 115]);
    day_to_proper.insert(24, vec![116, 117, 118, 1190]);
    day_to_proper.insert(25, vec![1191, 1192]);
    day_to_proper.insert(26, vec![1193, 1194]);
    day_to_proper.insert(
        27,
        vec![120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131],
    );
    day_to_proper.insert(28, vec![132, 133, 134, 135, 136, 137, 138]);
    day_to_proper.insert(29, vec![139, 140, 141, 142, 143]);
    day_to_proper.insert(30, vec![144, 145, 146, 147, 148, 149, 150]);
    day_to_proper.insert(31, vec![122, 134]);

    let proper = day_to_proper.get(&day).expect("the Vec of int");
    proper.to_vec()
}

fn print_new_coverdale(proper: Vec<u32>) {
    let _ps01 = include_str!("new_coverdale/ps01.txt");
    let _ps02 = include_str!("new_coverdale/ps02.txt");
    let _ps03 = include_str!("new_coverdale/ps03.txt");
    let _ps04 = include_str!("new_coverdale/ps04.txt");
    let _ps05 = include_str!("new_coverdale/ps05.txt");
    let _ps06 = include_str!("new_coverdale/ps06.txt");
    let _ps07 = include_str!("new_coverdale/ps07.txt");
    let _ps08 = include_str!("new_coverdale/ps08.txt");
    let _ps09 = include_str!("new_coverdale/ps09.txt");
    let _ps10 = include_str!("new_coverdale/ps10.txt");
    let _ps11 = include_str!("new_coverdale/ps11.txt");
    let _ps12 = include_str!("new_coverdale/ps12.txt");
    let _ps13 = include_str!("new_coverdale/ps13.txt");
    let _ps14 = include_str!("new_coverdale/ps14.txt");
    let _ps15 = include_str!("new_coverdale/ps15.txt");
    let _ps16 = include_str!("new_coverdale/ps16.txt");
    let _ps17 = include_str!("new_coverdale/ps17.txt");
    let _ps18 = include_str!("new_coverdale/ps18.txt");
    let _ps19 = include_str!("new_coverdale/ps19.txt");
    let _ps20 = include_str!("new_coverdale/ps20.txt");
    let _ps21 = include_str!("new_coverdale/ps21.txt");
    let _ps22 = include_str!("new_coverdale/ps22.txt");
    let _ps23 = include_str!("new_coverdale/ps23.txt");
    let _ps24 = include_str!("new_coverdale/ps24.txt");
    let _ps25 = include_str!("new_coverdale/ps25.txt");
    let _ps26 = include_str!("new_coverdale/ps26.txt");
    let _ps27 = include_str!("new_coverdale/ps27.txt");
    let _ps28 = include_str!("new_coverdale/ps28.txt");
    let _ps29 = include_str!("new_coverdale/ps29.txt");
    let _ps30 = include_str!("new_coverdale/ps30.txt");
    let _ps31 = include_str!("new_coverdale/ps31.txt");
    let _ps32 = include_str!("new_coverdale/ps32.txt");
    let _ps33 = include_str!("new_coverdale/ps33.txt");
    let _ps34 = include_str!("new_coverdale/ps34.txt");
    let _ps35 = include_str!("new_coverdale/ps35.txt");
    let _ps36 = include_str!("new_coverdale/ps36.txt");
    let _ps37 = include_str!("new_coverdale/ps37.txt");
    let _ps38 = include_str!("new_coverdale/ps38.txt");
    let _ps39 = include_str!("new_coverdale/ps39.txt");
    let _ps40 = include_str!("new_coverdale/ps40.txt");
    let _ps41 = include_str!("new_coverdale/ps41.txt");
    let _ps42 = include_str!("new_coverdale/ps42.txt");
    let _ps43 = include_str!("new_coverdale/ps43.txt");
    let _ps44 = include_str!("new_coverdale/ps44.txt");
    let _ps45 = include_str!("new_coverdale/ps45.txt");
    let _ps46 = include_str!("new_coverdale/ps46.txt");
    let _ps47 = include_str!("new_coverdale/ps47.txt");
    let _ps48 = include_str!("new_coverdale/ps48.txt");
    let _ps49 = include_str!("new_coverdale/ps49.txt");
    let _ps50 = include_str!("new_coverdale/ps50.txt");
    let _ps51 = include_str!("new_coverdale/ps51.txt");
    let _ps52 = include_str!("new_coverdale/ps52.txt");
    let _ps53 = include_str!("new_coverdale/ps53.txt");
    let _ps54 = include_str!("new_coverdale/ps54.txt");
    let _ps55 = include_str!("new_coverdale/ps55.txt");
    let _ps56 = include_str!("new_coverdale/ps56.txt");
    let _ps57 = include_str!("new_coverdale/ps57.txt");
    let _ps58 = include_str!("new_coverdale/ps58.txt");
    let _ps59 = include_str!("new_coverdale/ps59.txt");
    let _ps60 = include_str!("new_coverdale/ps60.txt");
    let _ps61 = include_str!("new_coverdale/ps61.txt");
    let _ps62 = include_str!("new_coverdale/ps62.txt");
    let _ps63 = include_str!("new_coverdale/ps63.txt");
    let _ps64 = include_str!("new_coverdale/ps64.txt");
    let _ps65 = include_str!("new_coverdale/ps65.txt");
    let _ps66 = include_str!("new_coverdale/ps66.txt");
    let _ps67 = include_str!("new_coverdale/ps67.txt");
    let _ps68 = include_str!("new_coverdale/ps68.txt");
    let _ps69 = include_str!("new_coverdale/ps69.txt");
    let _ps70 = include_str!("new_coverdale/ps70.txt");
    let _ps71 = include_str!("new_coverdale/ps71.txt");
    let _ps72 = include_str!("new_coverdale/ps72.txt");
    let _ps73 = include_str!("new_coverdale/ps73.txt");
    let _ps74 = include_str!("new_coverdale/ps74.txt");
    let _ps75 = include_str!("new_coverdale/ps75.txt");
    let _ps76 = include_str!("new_coverdale/ps76.txt");
    let _ps77 = include_str!("new_coverdale/ps77.txt");
    let _ps78 = include_str!("new_coverdale/ps78.txt");
    let _ps79 = include_str!("new_coverdale/ps79.txt");
    let _ps80 = include_str!("new_coverdale/ps80.txt");
    let _ps81 = include_str!("new_coverdale/ps81.txt");
    let _ps82 = include_str!("new_coverdale/ps82.txt");
    let _ps83 = include_str!("new_coverdale/ps83.txt");
    let _ps84 = include_str!("new_coverdale/ps84.txt");
    let _ps85 = include_str!("new_coverdale/ps85.txt");
    let _ps86 = include_str!("new_coverdale/ps86.txt");
    let _ps87 = include_str!("new_coverdale/ps87.txt");
    let _ps88 = include_str!("new_coverdale/ps88.txt");
    let _ps89 = include_str!("new_coverdale/ps89.txt");
    let _ps90 = include_str!("new_coverdale/ps90.txt");
    let _ps91 = include_str!("new_coverdale/ps91.txt");
    let _ps92 = include_str!("new_coverdale/ps92.txt");
    let _ps93 = include_str!("new_coverdale/ps93.txt");
    let _ps94 = include_str!("new_coverdale/ps94.txt");
    let _ps95 = include_str!("new_coverdale/ps95.txt");
    let _ps96 = include_str!("new_coverdale/ps96.txt");
    let _ps97 = include_str!("new_coverdale/ps97.txt");
    let _ps98 = include_str!("new_coverdale/ps98.txt");
    let _ps99 = include_str!("new_coverdale/ps99.txt");
    let _ps100 = include_str!("new_coverdale/ps100.txt");
    let _ps101 = include_str!("new_coverdale/ps101.txt");
    let _ps102 = include_str!("new_coverdale/ps102.txt");
    let _ps103 = include_str!("new_coverdale/ps103.txt");
    let _ps104 = include_str!("new_coverdale/ps104.txt");
    let _ps105 = include_str!("new_coverdale/ps105.txt");
    let _ps106 = include_str!("new_coverdale/ps106.txt");
    let _ps107 = include_str!("new_coverdale/ps107.txt");
    let _ps108 = include_str!("new_coverdale/ps108.txt");
    let _ps109 = include_str!("new_coverdale/ps109.txt");
    let _ps110 = include_str!("new_coverdale/ps110.txt");
    let _ps111 = include_str!("new_coverdale/ps111.txt");
    let _ps112 = include_str!("new_coverdale/ps112.txt");
    let _ps113 = include_str!("new_coverdale/ps113.txt");
    let _ps114 = include_str!("new_coverdale/ps114.txt");
    let _ps115 = include_str!("new_coverdale/ps115.txt");
    let _ps116 = include_str!("new_coverdale/ps116.txt");
    let _ps117 = include_str!("new_coverdale/ps117.txt");
    let _ps118 = include_str!("new_coverdale/ps118.txt");
    let _ps119a = include_str!("new_coverdale/ps119a.txt");
    let _ps119b = include_str!("new_coverdale/ps119b.txt");
    let _ps119c = include_str!("new_coverdale/ps119c.txt");
    let _ps119d = include_str!("new_coverdale/ps119d.txt");
    let _ps119e = include_str!("new_coverdale/ps119e.txt");
    let _ps120 = include_str!("new_coverdale/ps120.txt");
    let _ps121 = include_str!("new_coverdale/ps121.txt");
    let _ps122 = include_str!("new_coverdale/ps122.txt");
    let _ps123 = include_str!("new_coverdale/ps123.txt");
    let _ps124 = include_str!("new_coverdale/ps124.txt");
    let _ps125 = include_str!("new_coverdale/ps125.txt");
    let _ps126 = include_str!("new_coverdale/ps126.txt");
    let _ps127 = include_str!("new_coverdale/ps127.txt");
    let _ps128 = include_str!("new_coverdale/ps128.txt");
    let _ps129 = include_str!("new_coverdale/ps129.txt");
    let _ps130 = include_str!("new_coverdale/ps130.txt");
    let _ps131 = include_str!("new_coverdale/ps131.txt");
    let _ps132 = include_str!("new_coverdale/ps132.txt");
    let _ps133 = include_str!("new_coverdale/ps133.txt");
    let _ps134 = include_str!("new_coverdale/ps134.txt");
    let _ps135 = include_str!("new_coverdale/ps135.txt");
    let _ps136 = include_str!("new_coverdale/ps136.txt");
    let _ps137 = include_str!("new_coverdale/ps137.txt");
    let _ps138 = include_str!("new_coverdale/ps138.txt");
    let _ps139 = include_str!("new_coverdale/ps139.txt");
    let _ps140 = include_str!("new_coverdale/ps140.txt");
    let _ps141 = include_str!("new_coverdale/ps141.txt");
    let _ps142 = include_str!("new_coverdale/ps142.txt");
    let _ps143 = include_str!("new_coverdale/ps143.txt");
    let _ps144 = include_str!("new_coverdale/ps144.txt");
    let _ps145 = include_str!("new_coverdale/ps145.txt");
    let _ps146 = include_str!("new_coverdale/ps146.txt");
    let _ps147 = include_str!("new_coverdale/ps147.txt");
    let _ps148 = include_str!("new_coverdale/ps148.txt");
    let _ps149 = include_str!("new_coverdale/ps149.txt");
    let _ps150 = include_str!("new_coverdale/ps150.txt");

    for ps in proper.iter() {
        match ps {
            01 => print!("{}", _ps01),
            02 => print!("{}", _ps02),
            03 => print!("{}", _ps03),
            04 => print!("{}", _ps04),
            05 => print!("{}", _ps05),
            06 => print!("{}", _ps06),
            07 => print!("{}", _ps07),
            08 => print!("{}", _ps08),
            09 => print!("{}", _ps09),
            10 => print!("{}", _ps10),
            11 => print!("{}", _ps11),
            12 => print!("{}", _ps12),
            13 => print!("{}", _ps13),
            14 => print!("{}", _ps14),
            15 => print!("{}", _ps15),
            16 => print!("{}", _ps16),
            17 => print!("{}", _ps17),
            18 => print!("{}", _ps18),
            19 => print!("{}", _ps19),
            20 => print!("{}", _ps20),
            21 => print!("{}", _ps21),
            22 => print!("{}", _ps22),
            23 => print!("{}", _ps23),
            24 => print!("{}", _ps24),
            25 => print!("{}", _ps25),
            26 => print!("{}", _ps26),
            27 => print!("{}", _ps27),
            28 => print!("{}", _ps28),
            29 => print!("{}", _ps29),
            30 => print!("{}", _ps30),
            31 => print!("{}", _ps31),
            32 => print!("{}", _ps32),
            33 => print!("{}", _ps33),
            34 => print!("{}", _ps34),
            35 => print!("{}", _ps35),
            36 => print!("{}", _ps36),
            37 => print!("{}", _ps37),
            38 => print!("{}", _ps38),
            39 => print!("{}", _ps39),
            40 => print!("{}", _ps40),
            41 => print!("{}", _ps41),
            42 => print!("{}", _ps42),
            43 => print!("{}", _ps43),
            44 => print!("{}", _ps44),
            45 => print!("{}", _ps45),
            46 => print!("{}", _ps46),
            47 => print!("{}", _ps47),
            48 => print!("{}", _ps48),
            49 => print!("{}", _ps49),
            50 => print!("{}", _ps50),
            51 => print!("{}", _ps51),
            52 => print!("{}", _ps52),
            53 => print!("{}", _ps53),
            54 => print!("{}", _ps54),
            55 => print!("{}", _ps55),
            56 => print!("{}", _ps56),
            57 => print!("{}", _ps57),
            58 => print!("{}", _ps58),
            59 => print!("{}", _ps59),
            60 => print!("{}", _ps60),
            61 => print!("{}", _ps61),
            62 => print!("{}", _ps62),
            63 => print!("{}", _ps63),
            64 => print!("{}", _ps64),
            65 => print!("{}", _ps65),
            66 => print!("{}", _ps66),
            67 => print!("{}", _ps67),
            68 => print!("{}", _ps68),
            69 => print!("{}", _ps69),
            70 => print!("{}", _ps70),
            71 => print!("{}", _ps71),
            72 => print!("{}", _ps72),
            73 => print!("{}", _ps73),
            74 => print!("{}", _ps74),
            75 => print!("{}", _ps75),
            76 => print!("{}", _ps76),
            77 => print!("{}", _ps77),
            78 => print!("{}", _ps78),
            79 => print!("{}", _ps79),
            80 => print!("{}", _ps80),
            81 => print!("{}", _ps81),
            82 => print!("{}", _ps82),
            83 => print!("{}", _ps83),
            84 => print!("{}", _ps84),
            85 => print!("{}", _ps85),
            86 => print!("{}", _ps86),
            87 => print!("{}", _ps87),
            88 => print!("{}", _ps88),
            89 => print!("{}", _ps89),
            90 => print!("{}", _ps90),
            91 => print!("{}", _ps91),
            92 => print!("{}", _ps92),
            93 => print!("{}", _ps93),
            94 => print!("{}", _ps94),
            95 => print!("{}", _ps95),
            96 => print!("{}", _ps96),
            97 => print!("{}", _ps97),
            98 => print!("{}", _ps98),
            99 => print!("{}", _ps99),
            100 => print!("{}", _ps100),
            101 => print!("{}", _ps101),
            102 => print!("{}", _ps102),
            103 => print!("{}", _ps103),
            104 => print!("{}", _ps104),
            105 => print!("{}", _ps105),
            106 => print!("{}", _ps106),
            107 => print!("{}", _ps107),
            108 => print!("{}", _ps108),
            109 => print!("{}", _ps109),
            110 => print!("{}", _ps110),
            111 => print!("{}", _ps111),
            112 => print!("{}", _ps112),
            113 => print!("{}", _ps113),
            114 => print!("{}", _ps114),
            115 => print!("{}", _ps115),
            116 => print!("{}", _ps116),
            117 => print!("{}", _ps117),
            118 => print!("{}", _ps118),
            1190 => print!("{}", _ps119a),
            1191 => print!("{}", _ps119b),
            1192 => print!("{}", _ps119c),
            1193 => print!("{}", _ps119d),
            1194 => print!("{}", _ps119e),
            120 => print!("{}", _ps120),
            121 => print!("{}", _ps121),
            122 => print!("{}", _ps122),
            123 => print!("{}", _ps123),
            124 => print!("{}", _ps124),
            125 => print!("{}", _ps125),
            126 => print!("{}", _ps126),
            127 => print!("{}", _ps127),
            128 => print!("{}", _ps128),
            129 => print!("{}", _ps129),
            130 => print!("{}", _ps130),
            131 => print!("{}", _ps131),
            132 => print!("{}", _ps132),
            133 => print!("{}", _ps133),
            134 => print!("{}", _ps134),
            135 => print!("{}", _ps135),
            136 => print!("{}", _ps136),
            137 => print!("{}", _ps137),
            138 => print!("{}", _ps138),
            139 => print!("{}", _ps139),
            140 => print!("{}", _ps140),
            141 => print!("{}", _ps141),
            142 => print!("{}", _ps142),
            143 => print!("{}", _ps143),
            144 => print!("{}", _ps144),
            145 => print!("{}", _ps145),
            146 => print!("{}", _ps146),
            147 => print!("{}", _ps147),
            148 => print!("{}", _ps148),
            149 => print!("{}", _ps149),
            150 => print!("{}", _ps150),
            _ => (),
        };
    }
}
